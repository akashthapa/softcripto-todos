var options = {
    onCompleteButtonClick: function () {
        var id = this.getAttribute('data-id');
        var item = findItemById(id);
        item.isCompleted = !item.isCompleted;
        todo.start();
    },
    onDeleteButtonClick: function () {
        var id = this.getAttribute('data-id');
        var index = findIndexById(id);
        todos.splice(index, 1);
        todo.start();
    }
};

var todo = new TodoApp(options);//init
todo.start(); //call method
/**
 * Add Click Event to Save Button
 * - When user clicks save button it fires create() function
 */
var btn = document.getElementById('saveBtn');
btn.addEventListener('click', function(){
    todo.create();
});

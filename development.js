/**
 * 1. How to create an elment?
 * <li></li>
 */
var list = document.createElement("li");
/**
 * How to add a class to an element?
 * <li class="list-group-item"></li>
 */
list.classList.add('list-group-item');
/**
 * How to add text or paragraph
 * <li class="list-group-item">Hello me!!!</li>
 */
var text = document.createTextNode("Hello me!!!");
list.appendChild(text);
/**
 * How to select an element
 * <ul class="list-group" id="myTodoList">
 *  ...
 *  <li class="list-group-item">Hello me!!!</li>
 * </ul>
 */
var parent = document.getElementById("myTodoList");
parent.appendChild(list);
/**
 * How to add event listener
 */
list.addEventListener('click', function(event) {
    this.classList.add("bg-danger");
    this.classList.add("text-white");
});
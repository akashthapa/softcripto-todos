/**
 * Remove an item from an array
 * `Array.splice(index, count);`
 * 
 * Find Index of item
 * Array.findIndex(function (item) { return item.id == 1234; });
 */



var list = ['potato', 'tomato', 'rice', 'wheat'];
/**
 * Remove one item from list
 */
list.splice(1, 1); //deletes tomoto
console.log(list);


var todos = [
    {
        id: Date.now(),
        title: 'Sample task one',
        isCompleted: false
    },
    {
        id: Date.now() + 1,
        title: 'Sample task 2',
        isCompleted: false
    },
    {
        id: Date.now() + 2,
        title: 'Sample task 3',
        isCompleted: true
    }
];
/**
 * Find index of element in array
 */
function findIndexById(id) {
    return todos.findIndex(function (todo) {
        return todo.id == id;
    });
}
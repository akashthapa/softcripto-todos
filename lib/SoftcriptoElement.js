/**
 * Custom Object to handle HTML elements
 * @author Akash Thapa
 */
var SoftcriptoElement = {
    /**
     * @param {Ojbect} todo {id, title, isCompleted}
     * @param {function} callback - callback function
     */
    createCompleteButton: function (todo, callback) {
        var btnComplete = document.createElement('button');
        btnComplete.classList.add('btn');
        var buttonLabel = '';
        var buttonClass = '';

        if (todo.isCompleted) {
            buttonLabel = 'Undo';
            buttonClass = 'btn-success';
        } else {
            buttonLabel = 'Done';
            buttonClass = 'btn-info';
        }
        btnComplete.classList.add(buttonClass);
        btnComplete.appendChild(document.createTextNode(buttonLabel));
        btnComplete.setAttribute('data-id', todo.id); //set data-id 
        btnComplete.addEventListener('click', callback);
        return btnComplete;
    },
    createDeleteButton: function (todo, callback) {
        var btnDelete = document.createElement('button');
        btnDelete.classList.add('btn');
        btnDelete.classList.add('btn-danger');
        btnDelete.appendChild(document.createTextNode('Delete'));
        btnDelete.addEventListener('click', callback);
        btnDelete.setAttribute('data-id', todo.id); //set data-id 
        return btnDelete;
    }
};
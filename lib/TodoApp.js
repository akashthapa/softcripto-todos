/**
 * @param {object} options - todo app configuration
 * @constructor
 */
function TodoApp(options){
    this.start = function(){
        this.reset(); //reset 
        for(var i = 0; i < todos.length; i++){
            var todo = todos[i];
            this.addItem(todo);
        }
    }
    this.reset = function(){
        var list = document.querySelectorAll('#myTodoList tr.todo');
        for(var i = 0; i < list.length; i++){
            var node = list[i];
            node.remove();
        }
    }
    this.addItem = function(todo){
        var table = document.getElementById("myTodoList"); //
        var tr = document.createElement('tr');
        var td1 = document.createElement('td');
        var td2 = document.createElement('td');
        tr.classList.add('todo'); //add class to TR
        //td 1
        var text = document.createTextNode(todo.title);
        td1.appendChild(text);
        //td 2
        td2.appendChild(SoftcriptoElement.createCompleteButton(todo, options.onCompleteButtonClick));
        td2.appendChild(SoftcriptoElement.createDeleteButton(todo, options.onDeleteButtonClick));

        tr.appendChild(td1);
        tr.appendChild(td2);
        table.appendChild(tr);
    }
    this.create = function(){
        var input = document.getElementById("inputText");
        todos.unshift({
            title: input.value,
            isCompleted: false,
            id: Date.now()
        });
        this.start();
        input.value = '';
        input.focus();
    }
}

/**
 * File separation
 * We can create as many file as we want
 * File must be loaded in priority order.
 */
var todos = [
    {
        id: Date.now(),
        title: 'Sample task one',
        isCompleted: false
    },
    {
        id: Date.now() + 1,
        title: 'Sample task 2',
        isCompleted: false
    },
    {
        id: Date.now() + 2,
        title: 'Sample task 3',
        isCompleted: true
    },
    {
        id: Date.now() + 6,
        title: 'Sample task 4567',
        isCompleted: false
    },
];
/**
 * Returns object item from todos
 * @param {String|Number} id id of an item
 */
function findItemById(id) {
    return todos.find(function (todo) {
        return todo.id == id;
    });
}
/**
 * Returns index of an item
 * @param {String|Number} id 
 */
function findIndexById(id) {
    return todos.findIndex(function (todo) {
        return todo.id == id;
    });
}